/**Task 2
 Написать функцию makeRandomFn которя принимает диапазон чисел и возвращяет функцию,
 которая при вызове возвращяет случайное число c этого диапазона. */

const getRandomNumber = makeRandomFn([1, 2, 100, 34, 45, 556, 33]);
console.log(getRandomNumber());// 556
console.log(getRandomNumber());// 100
console.log(getRandomNumber()); // 2

function makeRandomFn(data) {
    return function () {
        if (isIntegerNumber(data)) {
            return data[Math.floor(Math.random() * data.length)];
        }

        return null;
    }
}

function isIntegerNumber(data) {
    return data.filter(number => !Number.isInteger(number)).length === 0;
}