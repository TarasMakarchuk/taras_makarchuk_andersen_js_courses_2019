/**Task 1
 Ваша задача - заполнить пробелы (_______________________).
 Функция multiplier принимает x один параметр и возвращает анонимную функцию,
 которая принимает один параметр - y, и возвращает произведение x * y. */

function multiplier(x) {
    return function (y) {
        return x * y;
    }
}

function processData(input) {
    const waterWeight = multiplier(1000);
    const mercuryWeight = multiplier(1355);
    const oilWeight = multiplier(900);

    console.log("Weight of " + input + " metric cube of water = " + waterWeight(input) + " kg");
    console.log("Weight of " + input + " metric cube of mercury = " + mercuryWeight(input) + " kg");
    console.log("Weight of " + input + " metric cube of oil = " + oilWeight(input) + " kg");
}

processData(5);