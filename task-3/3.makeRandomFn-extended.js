/**Task 3
 Нужно расширить функцию makeRandomFn, таким образом
 чтобы можно было передавать диапазон не только через масив, а как аргументы через запятую*/

makeRandomFn(1, 2, 100, 34, 45, 556, 33, 'a');
makeRandomFn(1, 2, 100, 34, 45, 556, 33);
makeRandomFn([1, 2, 100, 34, 45, 556, 33, 'b']);
makeRandomFn([1, 2, 100, 34, 45, 556, 33]);

function makeRandomFn(...data) {
    data = data.reduce((first, last) => first.concat(last), []);
    if (isIntegerNumber(data)) {
        return data[Math.floor(Math.random() * data.length)];
    }

    return null;
}

function isIntegerNumber(data) {
    return data.filter(number => !Number.isInteger(number)).length === 0;
}