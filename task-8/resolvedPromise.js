/** ●    Написать функцию, принимающую список url путей и возвращающую промис, который резолвит
 результаты всех запросов по указанным url (важно: резолвит массив значений, резолвит не раньше
 последнего из запросов, функцию запроса взять из ссылки на gist, не использовать Promise.all).
 ●    P.S. список url - так как функция запроса фейковая, то под списком url стоить понимать любой
 массив строк.
 ●    https://gist.github.com/antonsavchenko94/fa2347b7bd92c1f527fd29402538a965 */


const linksList = [
    'learn.javascript.ru',
    'developer.mozilla.org',
    'medium.com',
    'habr.com',
    'github.com',
    'bitbucket.org'
];

function request(url) {
    return new Promise((res, rej) => {
        const delayTime = Math.floor(Math.random() * 10000) + 1;

        setTimeout(() => res(url), delayTime);
    });
}

resolvedPromise(linksList);

function resolvedPromise(links) {
    const newLinksArray = [];
    if (links && !isFinite(links)) {
        return new Promise((resolve, reject) => {
            links.forEach((data) => {
                request(data).then(() => {
                    newLinksArray.push(data);
                    resolve(newLinksArray);

                    return newLinksArray;
                });
            });
        });
    }
    throw new Error(`error links`);
}