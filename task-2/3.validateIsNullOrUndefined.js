/**Напишите функцию, которая принимает 1 параметр любого типа и
 проверяет, является ли полученное значение null или undefined.*/

console.log(validateIsNullOrUndefined(undefined));
console.log(validateIsNullOrUndefined(null));
console.log(validateIsNullOrUndefined('Hello World'));
console.log(validateIsNullOrUndefined(12345));
console.log(validateIsNullOrUndefined(true));

function validateIsNullOrUndefined(parameter) {
    return parameter === null || parameter === undefined;
}