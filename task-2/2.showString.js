/**Напишите функцию, которая принимает 1 параметр - строку и
 возвращает новую строку вида, “Вы ввели * полученная строка *”.*/

var greeting = 'Hello world!';
var number = 123;
var undefinedValue = undefined;
var nullValue = null;

console.log(showString(greeting));
console.log(showString(number));
console.log(showString(undefinedValue));
console.log(showString(nullValue));

function showString(string) {
    if (typeof string === 'string') {
        return 'Вы ввели ' + '*' + string + '*';
    }
    return 'введите строку';
}