/**Напишите функцию, которая принимает 1 параметр - число и выводит в
 консоль все числа от нуля до введенного числа (в обе стороны).*/

showResult(9);
console.log('');
showResult(4.89898754375435878748573485);
console.log('');
showResult(null);
showResult(NaN);
showResult(undefined);
showResult(Infinity);
showResult('string');

function showResult(number) {
    if (typeof number === 'number' && isFinite(number)) {
        number = number ^ 0;
        printNumbersFrom0toN(number);
        printNumbersFromNto0(number);
    } else {
        console.log('enter a number');
    }
}

function printNumbersFrom0toN(number) {
    for (var i = 0; i <= number; i++) {
        console.log(i);
    }
}

function printNumbersFromNto0(number) {
    for (var j = number; j >= 0; j--) {
        console.log(j);
    }
}