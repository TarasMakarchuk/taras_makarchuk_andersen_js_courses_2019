/**1. Напишите функцию, которая принимает 2 параметра - 2 числа и
 возвращает true, если первое число больше второго, и false, если это не
 так.*/

console.log(compareNumbers(1, 2));
console.log(compareNumbers(2, 1));
console.log(compareNumbers('2', 1));
console.log(compareNumbers(Boolean, NaN));
console.log(compareNumbers(undefined, null));
console.log(compareNumbers(true, false));
console.log(compareNumbers(NaN, Infinity));

function compareNumbers(firstNumber, lastNumber) {
    return validate(firstNumber, lastNumber) ? firstNumber > lastNumber : 'enter a number';
}

function validate(first, last) {
    return isNumber(first, last) &&
        isFinite(first) &&
        isFinite(last);
}

function isNumber(first, last) {
    return typeof first === 'number' && typeof last === 'number';
}