/**Напишите функцию, которая принимает 1 параметр - объект и добавляет
 этому объекту поле checked со значением true.*/

var crow = {};
var dog = {
    firstName: 'Sharik',
    lastName: 'Bark-off',
    voice: 'RrrRrr'
};
var number = 1;
var nullValue = null;

console.log(addFieldCheckedToObject(crow));
console.log(crow);
console.log(addFieldCheckedToObject(dog));
console.log(dog);
console.log(addFieldCheckedToObject(number));
console.log(number);
console.log(addFieldCheckedToObject(nullValue));
console.log(nullValue);

function addFieldCheckedToObject(object) {
    return (typeof object === 'object' && object) ? object.checked = true : object;
}