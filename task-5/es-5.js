/** Task 1.
 •    Создайте функцию - конструктор Person. Каждый Person должен иметь:
 o    свойства firstname, lastname и age;
     firstname и lastname всегда должны быть строки от 3 до 20 символов, содержащие только латинские буквы;
     age всегда должно быть числом в диапазоне (0, 150) включительно;
     если что-либо из перечисленного не выполнено, выкиньте ошибку;
 o    свойство fullname:
     геттер возвращает строку в формате 'FIRST_NAME LAST_NAME';
     сэттер receives a string is the format 'FIRST_NAME LAST_NAME';
     он должен разобрать его и установить firstname и lastname;
 o    метод introduce() который возвращает строку в формате 'Hello! My name is FULL_NAME and I am AGE-years-old' */

function Person(firstName, lastName, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
}

Person.prototype.validateAge = function () {
    const minimalAge = 0;
    const maximalAge = 150;
    try {
        if (this.age < minimalAge) {
            throw `age is less than ${minimalAge}`;
        }
        if (this.age > maximalAge) {
            throw `age is more than ${maximalAge}`;
        }
    } catch (error) {
        throw new Error(error);
    }
};

Person.prototype.validateFirstName = function () {
    validateNameLength(this.firstName);
};

Person.prototype.validateLastName = function () {
    validateNameLength(this.lastName);
};

Person.prototype.validateFirstNameLatin = function () {
    validateNameLatin(this.firstName);
};

Person.prototype.validateLastNameLatin = function () {
    validateNameLatin(this.lastName);
};

Person.prototype.validateFullName = function () {
    this.validateFirstNameLatin();
    this.validateLastNameLatin();
};

Person.prototype.setFullName = function (fullName) {
    const splitName = fullName.split(' ');
    this.firstName = splitName[0];
    this.lastName = splitName[1];
    this.validateFullName();
};

Person.prototype.getFullName = function () {
    return this.firstName.toUpperCase() + ' ' + this.lastName.toUpperCase();
};

Person.prototype.introduce = function () {
    return `Hello! My name is ${this.getFullName()} and I am ${this.age}-years-old`;
};

function validateNameLength(name) {
    const minimalNameLength = 3;
    const maximalNameLength = 20;
    try {
        if (name.length < minimalNameLength) {
            throw `${name} is less than ${minimalNameLength}`;
        }
        if (name.length > maximalNameLength) {
            throw `${name} is more than ${maximalNameLength}`;
        }
    } catch (error) {
        throw new Error(error);
    }
}

function validateNameLatin(name) {
    const regularExpression = /^[a-zA-Z]+$/;
    try {
        if (!regularExpression.test(name)) {
            throw `the name ${name} contains non-latin letters`;
        }
    } catch (error) {
        throw new Error(error);
    }
}

const person = new Person('Bruce', 'Willis', 1);
console.log(`${person.firstName} ${person.lastName} ${person.age}`);

console.log(person);
person.validateAge();
person.validateFirstName();
person.validateLastName();
person.validateFirstNameLatin();
person.validateLastNameLatin();
person.setFullName('Jackie Chan');
person.getFullName();
console.log(person.introduce());
console.log(person);


/** Task 2.
 •    Создайте конструктор Worker. Worker наследуется от Person - то есть будет иметь теже поля и
 методы, что и Person.
 o    У Worker будет свойство experience - стаж в годах;
     experience всегда должно быть числом в диапазоне (1, 50) включительно;
 o    добавить ствойство `salary`:
     всегда должно быть числом в диапазоне (1000, 10000) включительно;
 o    свойство remunerationRate - рейт вознаграждения за выслугу лет:
     геттер возвращает число, которое вычисляется по формуле experience * P / 100
     где P это процент, который зависит от experience, если experience
     в диапозоне диапозону 1-5, то P = 5
     в диапозоне диапозону 6-10, то P = 10
     в диапозоне диапозону 11-20, то P = 20
     если больше 20, то Р = 50
     сэттера у этого поля не будет */

function Worker(firstName, lastName, age) {
    Person.apply(this, arguments);
}

Worker.prototype = Object.create(Person.prototype);
Worker.prototype.constructor = Worker;

Worker.prototype.setExperience = function (experience) {
    const minimalExperience = 1;
    const maximalExperience = 50;
    try {
        if (experience < minimalExperience) {
            throw `experience is less than ${minimalExperience}`;
        }
        if (experience > maximalExperience) {
            throw `experience is more than ${maximalExperience}`;
        }
        this.experience = experience;
    } catch (error) {
        throw new Error(error);
    }
};

Worker.prototype.getExperience = function () {
    return this.experience;
};

Worker.prototype.setSalary = function (salary) {
    const minimalSalary = 1000;
    const maximalSalary = 10000;
    try {
        if (salary < minimalSalary) {
            throw `salary is less than ${minimalSalary}`;
        }
        if (salary > maximalSalary) {
            throw `salary is more than ${maximalSalary}`;
        }
        this.salary = salary;
    } catch (error) {
        throw new Error(error);
    }
};

Worker.prototype.getSalary = function () {
    return this.experience;
};

Worker.prototype.getRemunerationRate = function () {
    const percentOneHundred = 100;
    const percentLow = 5;
    const percentMiddle = 10;
    const percentHigh = 20;
    const percentVeryHigh = 50;
    const experience = this.getExperience();
    switch (true) {
        case checkRange(experience, 1, 5):
            return experience * percentLow / percentOneHundred;
        case checkRange(experience, 6, 10):
            return experience * percentMiddle / percentOneHundred;
        case checkRange(experience, 11, 20):
            return experience * percentHigh / percentOneHundred;
        case checkRange(experience, 21, 50):
            return experience * percentVeryHigh / percentOneHundred;
        default:
            return 0;
    }
};

function checkRange(experience, minimum, maximum) {
    return experience >= minimum && experience <= maximum;
}

const engineer = new Worker('Stepan', 'Stepanovich', 23);

console.log(engineer);
engineer.validateAge();
engineer.validateFirstName();
engineer.validateLastName();
engineer.validateFirstNameLatin();
engineer.validateLastNameLatin();
engineer.setExperience(2);
console.log(engineer.getExperience());
engineer.setSalary(1000);
console.log(engineer.getSalary());
console.log('remuneration rate: ' + engineer.getRemunerationRate());
console.log(engineer);
