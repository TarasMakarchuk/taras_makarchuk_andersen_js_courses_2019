const wrapper = document.createElement('div');
wrapper.classList.add('wrapper');

const headerBlock = document.createElement('div');
headerBlock.classList.add('header-block');

const header = document.createElement('h2');
header.innerText = 'Basic calculator';
headerBlock.appendChild(header);

const bodyCalculator = document.createElement('div');
bodyCalculator.classList.add('body-calculator');

const display = document.createElement('input');
display.classList.add('display');
display.setAttribute('readonly', 'readonly');
display.value = '';
bodyCalculator.appendChild(display);

const historyContainer = document.createElement('div');
historyContainer.classList.add('history-container');

const headerHistory = document.createElement('h2');
headerHistory.classList.add('header-history');
headerHistory.innerText = 'History';
historyContainer.appendChild(headerHistory);

document.body.appendChild(wrapper);
wrapper.appendChild(headerBlock);
wrapper.appendChild(bodyCalculator);
wrapper.appendChild(historyContainer);

let button;

DATA.arraySymbols.forEach((element, index) => createButton(index));

bodyCalculator.addEventListener('click', function (event) {
    const target = event.target;
    if (target.tagName === 'BUTTON') {
        verifyData(target);
    }
});

function createButton(index) {
    button = document.createElement('button');
    button.classList.add('button');
    button.classList.add(DATA.arraySymbols[index]);
    bodyCalculator.appendChild(button);
    button.innerText = DATA.arraySymbols[index];
}

function verifyData(button) {
    const buttonsValue = +button.innerText;
    if (DATA.finalResult) {
        DATA.firstNumber = '';
        DATA.secondNumber = '';
        DATA.operator = '';
        DATA.finalResult = '';
    }
    if (Number.isInteger(buttonsValue) && !DATA.operator) {
        DATA.firstNumber = DATA.firstNumber + button.innerText;
        display.value = DATA.firstNumber;
    } else if (Number.isInteger(buttonsValue)) {
        DATA.secondNumber = DATA.secondNumber.toString() + button.innerText;
        display.value = DATA.secondNumber;
    } else if (button.innerText === '+'
        || button.innerText === '-'
        || button.innerText === '*'
        || button.innerText === '/') {
        if (DATA.firstNumber && DATA.secondNumber && DATA.operator) {
            calculateResult();
            DATA.result = '';
        }
        DATA.operator = button.innerText;
        display.value = DATA.operator;
    } else if (button.innerText === DATA.equally) {
        calculateResult();
        DATA.firstNumber = '';
        DATA.operator = '';
        DATA.finalResult = DATA.result;
        DATA.result = '';
    } else if (button.innerText === DATA.clear) {
        DATA.firstNumber = '';
        DATA.secondNumber = '';
        display.value = '';
        DATA.operator = '';
        DATA.result = '';
    }
}

function calculateResult() {
    switch (DATA.operator) {
        case '+':
            DATA.result = +DATA.firstNumber + +DATA.secondNumber;
            break;
        case '-':
            DATA.result = +DATA.firstNumber - +DATA.secondNumber;
            break;
        case '*':
            DATA.result = +DATA.firstNumber * +DATA.secondNumber;
            break;
        case '/':
            DATA.result = (+DATA.firstNumber / +DATA.secondNumber).toFixed(2);
            break;
        default:
            DATA.result = '';
    }
    display.value = DATA.result;
    const dataHistory = new Date().toString().slice(3, 25) + ': ' + DATA.firstNumber +
        DATA.operator + DATA.secondNumber + ' = ' + DATA.result + ';';
    createHistory(dataHistory);
    DATA.firstNumber = DATA.result;
    DATA.secondNumber = '';
}

function createHistory(data) {
    const history = document.createElement('div');
    history.classList.add('history');
    historyContainer.appendChild(history);
    history.innerText = data;
}