const DATA = {
    firstNumber: '',
    secondNumber: '',
    operator: '',
    result: '',
    equally: '=',
    clear: 'CE',
    arraySymbols: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, '+', '-', '/', '*', 'CE', '='],
    finalResult: ''
};