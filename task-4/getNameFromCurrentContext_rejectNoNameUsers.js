/**
 Task 1
 Написать функцию getNameFromCurrentContext, которая будет брать значение поля name из своего контекста вызова.
 Далее реализуем функцию rejectNoNameUsers, в которую передаем массив users - проходимся по каждому елементу массива
 users,  и в контексте каждого юзера вызываем функцию getNameFromCurrentContext. Если функция getNameFromCurrentContext
 вернула  undefined, тогда удалаем юзера из массива. Результатом работы rejectNoNameUsers будет НОВЫЙ массив юзеров,
 у которых есть поле name.
 */

// Масcив юзеров

const users = [
    {
        balance: '3946.45',
        picture: 'http://placehold.it/32x32',
        age: 23,
        name: 'Bird Ramsey',
        gender: 'male',
        company: 'NIMON',
        email: 'birdramsey@nimon.com',
    },
    {
        balance: '2499.49',
        picture: 'http://placehold.it/32x32',
        age: 31,
        gender: 'female',
        company: 'LUXURIA',
        email: 'lillianburgess@luxuria.com',
    },
    {
        balance: '2820.18',
        picture: 'http://placehold.it/32x32',
        age: 34,
        name: 'Kristie Cole',
        gender: 'female',
        company: 'QUADEEBO',
        email: 'kristiecole@quadeebo.com',
    },
    {
        balance: '1277.32',
        picture: 'http://placehold.it/32x32',
        age: 30,
        gender: 'female',
        company: 'GRONK',
        email: 'leonorcross@gronk.com',
    },
    {
        balance: '1972.47',
        picture: 'http://placehold.it/32x32',
        age: 28,
        gender: 'male',
        company: 'ULTRIMAX',
        email: 'marshmccall@ultrimax.com',
    },
];

rejectNoNameUsers(users);

function getNameFromCurrentContext(data) {
    return function () {
        return data.name;
    }
}

/** вариант 1 */
function rejectNoNameUsers(data) {
    const newUsers = [];
    for (let i = 0; i < data.length; i++) {
        if (getNameFromCurrentContext.call(data, data[i])(data) !== undefined) {
            newUsers.push(data[i]);
        }
    }

    return newUsers;
}

/** вариант 2 */
/*function rejectNoNameUsers(data) {
    const newUsers = [];
    data.forEach(function (elem) {
        if (getNameFromCurrentContext.call(data, elem)(data) !== undefined) {
            newUsers.push(elem);
        }
    });

    return newUsers;
}*/

/** вариант 3 */
/*function rejectNoNameUsers(data) {
    return data.filter(function (elem) {
        return getNameFromCurrentContext.call(data, elem)(data) !== undefined;
    });
}*/