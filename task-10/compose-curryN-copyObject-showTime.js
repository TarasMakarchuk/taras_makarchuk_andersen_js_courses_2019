/** Создать функцию compose которая выполняет композицию функций справа налево.
 compose(fn1, fn2, fn3, ... fnN), тоже самое что и fn1(fn2(fn3(...fnN(1)))) */

function plusOne(x) {
    return x + 1;
}

function negative(x) {
    return -x;
}

const compose = (...firstArgument) =>
    (...secondArgument) =>
        firstArgument.reduceRight((result, function_) =>
            [function_(...result)], secondArgument).toString();

const resultCompose = compose(plusOne, negative, Math.pow);
console.log(resultCompose(3, 4));


/** Написать функцию curryN, которая каррирует N-е количество аргументов переданной функции */

const curryN = (arity, function_) => {
    return function showFirstArgument(...firstArgument) {
        if (firstArgument.length >= arity) {
            return function_(...firstArgument);
        } else {
            return function showLastArgument(...lastArgument) {
                const newArgument = firstArgument.concat(lastArgument);
                return showFirstArgument(...newArgument);
            }
        }
    }
};

function printFullName(firstName, lastName, middleName) {
    console.log(`${firstName} ${lastName} ${middleName}`);
}

const curriedPrintFullName = curryN(3, printFullName);

curriedPrintFullName('Марин')('Сергеевич')('Мокану');


/** Написать функцию, что будет “глубоко” копировать объект. То есть,
 должны сделать копию не только объекта, а и его вложенностей */

const object = {
    a: 10,
    bbb: {
        b: 17,
        c: 15,
        d: {
            e: 22,
            f: 'hello',
            g: ['a', 'r', 'r', 'a', 'y'],
            h: {
                i: {
                    author: 'Stendal',
                    world: {
                        first: 'second'
                    }
                }
            }
        }
    }
};

function copyObject(object) {
    if (typeof object !== 'object' || object === null) {
        return object;
    } else if (object instanceof Array) {
        return object.reduce((array, item, index) => {
            array[index] = copyObject(item);
            return array;
        }, []);
    } else if (object instanceof Object) {
        return Object.keys(object).reduce((newObject, key) => {
            newObject[key] = copyObject(object[key]);
            return newObject;
        }, {})
    }
}

console.log(copyObject(object));


/** Написать функцию которая будет каждую минуту выводить текущие время в формате hh:mm */

const date = new Date();
const showHours = () => date.getHours();
const showMinutes = () => date.getMinutes();
const showTime = () => console.log(`${showHours()}:${showMinutes()}`);

setInterval(showTime, 60000);