/** // Task 1.
 Создайте функцию, которая на вход получаете строку "a.b.c.d",
 а на выходе возвращает объект: { a: { b: { c: { d: null } } } } */


function convertStringToObject(dataString) {
    const result = {};
    const array = dataString.split('.');
    array.reduce(function (key, value, index) {
        if (index === array.length - 1) {
            return key[value] = null;
        }
        return key[value] = {};
    }, result);

    return result;
}

console.log(convertStringToObject('a.b.c.d'));


/** Task 2
 Написать функцию, которая находит пересечение двух массивов.
 Например - результатом вызова
 findEquals([2, 6, 3, 7, 5], [1, 2, 3, 7]) будет масив [2, 3, 7] */


function findEquals(firstArray, secondArray) {
    return firstArray.filter(value => secondArray.includes(value));
}

console.log(findEquals([2, 6, 3, 7, 5], [1, 2, 3, 7]));


/** Task 3
 Напишите функцию для объединения двух массивов и удаления всех дублирующих элементов.
 const firstArray = [1, 2, 3];
 const secondArray = [2, 30, 1];
 const mergedArray = mergeArray(firstArray, secondArray); // [1, 2, 30, 3] */


const firstArray = [1, 2, 3];
const secondArray = [2, 30, 1];
const mergedArray = mergeArray(firstArray, secondArray);

function mergeArray(firstArray, secondArray) {
    const merged = firstArray.concat(secondArray);

    return merged.filter(function (item, position) {
        return merged.indexOf(item) === position;
    });
}

console.log(mergedArray);


/** Task 4
 Напишите функцию mapObject, которая преобразовывает значения
 всех свойств объекта (аналог map из массива, только для объекта, возвращает новый объект)
 const double = x => x * 2;
 mapObject(double, {x: 1, y: 2, z: 3}); //=> {x: 2, y: 4, z: 6} */


const double = x => x * 2;
const object = {x: 1, y: 2, z: 3};

//variant a
function mapObject(callback, data) {
    const arrayValues = Object.values(data);
    const arrayKeys = Object.keys(data);

    return Object.assign({}, ...arrayValues
        .map((value, index) => {
            return ({[arrayKeys[index]]: callback(arrayValues[index])})
        }));
}

//variant b
// function mapObject(callback, data) {
//     for (let key in data) {
//         data[key] = callback(data[key]);
//     }
//
//     return data;
// }

console.log(mapObject(double, object));