/**Написать функцию debounce, которая возвращает функцию-обертку,
 передающую оригинальной функции только последний вызов функции-обертки за переданный
 интервал. Примеры вызовов смотрите на
 https://gist.github.com/antonsavchenko94/d787b0b5760d7cafc5e48479057f17d2 */


function log(arguments) {
    console.log(arguments);
}

let newTime = null;

function debounce(callback, time) {
    let timerId = null;

    return function (data) {
        if (timerId) {
            clearTimeout(timerId);
            newTime += time;
        }
        timerId = setTimeout(function () {
            callback.call(this, data);
        }, newTime);
    }
}

var debouncedFirst = debounce(log, 500);
var debouncedSecond = debounce(log, 500);
var debouncedThird = debounce(log, 500);

debouncedFirst('1');
debouncedFirst('2');
// --- 500 ms ---
// '2'
debouncedSecond('3');
// --- 500 ms ---
// '3'
debouncedThird('4');
debouncedThird('5');
debouncedThird('6');
// --- 500 ms ---
// '6'